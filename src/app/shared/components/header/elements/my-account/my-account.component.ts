import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/firebase/auth.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

import { ProfileService } from 'src/app/core/services/profile/profile.service';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {

  public userName: string;
  public profileImg: 'assets/images/dashboard/profile.jpg';
  openMenu:boolean = false;
  userProfile: any;

  constructor(
    public authService: AuthService,
    private _authenticationService: AuthenticationService,
    private profileService: ProfileService
  ) {

    if (JSON.parse(localStorage.getItem('user'))) {
      console.log("true");
    } else {
      console.log("NO ");
    }

  }


  logoutFunc() {
    // this.authService.SignOut();
    this._authenticationService.logout()
  }

  ngOnInit() {

    this.profileService.getProfile$()
    .subscribe(data => {
      this.userName = data.name
      this.profileImg = data.profile_pic
    })
  }

  aktif() {
    this.openMenu = !this.openMenu;
  }

}
