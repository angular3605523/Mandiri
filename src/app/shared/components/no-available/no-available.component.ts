import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'no-available',
  templateUrl: './no-available.component.html',
  styleUrls: ['./no-available.component.scss']
})
export class NoAvailableComponent implements OnInit {

  @Input() title: string;
  constructor() { }

  ngOnInit(): void {
  }

}
