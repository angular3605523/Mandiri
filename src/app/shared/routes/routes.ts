import { Routes } from '@angular/router';
var psn = "SIPMA –  ";

export const content: Routes = [
 
  {
    path: "dashboard",
    title: psn +'Dashboard',
    loadChildren: () =>
      import("../../modules/dashboard/dashboard.module").then(
        (m) => m.DashboardModule
      ),
  }
];
