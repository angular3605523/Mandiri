import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  private REST_API_SERVER = environment.apiUrl;
  private employeesUrl = '/assets/employee.json';
  constructor(private http: HttpClient) { }

    getCustomersMedium() {
        return this.http.get<any>('/assets/employee.json')
    }

    // addData(newData: any): Observable<any> {
    //   return this.http.get<any>('/assets/employee.json').pipe(
    //     map((data) => {
    //       let newData1 = {
    //         username: newData.username,
    //         firstName: newData.firstName,
    //         lastName: newData.lastName,
    //         email: newData.email,
    //         birthDate: newData.birthDate,
    //         basicSalary: newData.basicSalary,
    //         age: 20, 
    //         status: newData.status,
    //         group: newData.group,
    //         description:newData.description
    //       }
    //       data.push(newData1);
    //       return data;
    //     }),
    //     switchMap((data) => {
    //       return this.http.put('/assets/employee.json', {data});
    //     })
    //   );
    // }

    addData(newData: any): Observable<any> {
      return this.http.get<any>('/assets/employee.json').pipe(
        map((data) => {
          data.data.push(newData);
          return data;
        }),
        tap((data) => {
          this.http.put('/assets/employee.json', data).subscribe();
        })
      );
    }
    
}
