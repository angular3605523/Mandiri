import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PackageService {

  private REST_API_SERVER = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAll$(): Observable<any> {
    return this.http.get(this.REST_API_SERVER + 'cloud-package/list');
  }
}
