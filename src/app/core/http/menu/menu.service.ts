import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class MenuService {
  private REST_API_SERVER = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAll$(): Observable<any> {
    return this.http.get(this.REST_API_SERVER + 'navigation');
  }
}
