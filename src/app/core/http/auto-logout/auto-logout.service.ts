import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

const MINUTES_UNITL_AUTO_LOGOUT = 30; // in Minutes
const CHECK_INTERVALL = 5000; // in ms
const STORE_KEY = 'wsc-cloud-last-action';

@Injectable({
  providedIn: 'root',
})
export class AutoLogoutService {
  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private ngZone: NgZone
  ) {
    this.reset();
    this.check();
    this.initListener();
    this.initInterval();
  }

  get lastAction() {
    const time = localStorage.getItem(STORE_KEY);
    if (time) {
      return parseInt(time);
    }

    return Date.now();
  }

  set lastAction(value) {
    localStorage.setItem(STORE_KEY, value.toString());
  }

  initListener() {
    this.ngZone.runOutsideAngular(() => {
      document.body.addEventListener('click', () => this.reset());
    });
  }

  initInterval() {
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        this.check();
      }, CHECK_INTERVALL);
    });
  }

  reset() {
    this.lastAction = Date.now();
  }

  check() {
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;

    this.ngZone.run(() => {
      if (isTimeout && this.auth.isLoggedIn()) {
        this.auth.logout();
        this.router.navigate(['auth/login']);
      }
    });
  }
}
