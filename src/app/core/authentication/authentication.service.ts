import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private REST_API_SERVER = environment.apiUrl;

  constructor(private router: Router, private http: HttpClient) {}

  setToken(token: string): void {
    localStorage.setItem('wsc-cloud-sctoken', token);
  }
  getToken(): string | null {
    return localStorage.getItem('wsc-cloud-sctoken');
  }
  
  setDarkLight(template: string): void {
    localStorage.setItem('dark-light-mode', template);
  }

  getDarkLight(): string | null {
    return localStorage.getItem('dark-light-mode');
  }

  setName(name: string): void {
    localStorage.setItem('wsc-cloud-name', name);
  }

  getName(): string | null {
    return localStorage.getItem('wsc-cloud-name');
  }

  setUserName(username: string): void {
    localStorage.setItem('wsc-cloud-username', username);
  }

  getUserName(): string | null {
    return localStorage.getItem('wsc-cloud-username');
  }

  setRole(role: string): void {
    localStorage.setItem('wsc-cloud-role', role);
  }

  getRoleId(): string | null {
    return localStorage.getItem('wsc-cloud-role-id');
  }

  setRoleId(role: string): void {
    localStorage.setItem('wsc-cloud-role-id', role);
  }

  getRole(): string | null {
    return localStorage.getItem('wsc-cloud-role');
  }

  setSite(site: string): void {
    localStorage.setItem('wsc-cloud-site', site);
  }

  getSite(): string | null {
    return localStorage.getItem('wsc-cloud-site');
  }

  setUserId(site: string): void {
    localStorage.setItem('wsc-cloud-user-id', site);
  }

  gettUserId(): string | null {
    return localStorage.getItem('wsc-cloud-user-id');
  }

  setImage(image: string): void {
    localStorage.setItem('wsc-cloud-img-user', image);
  }

  getImage(): string | null {
    return localStorage.getItem('wsc-cloud-img-user');
  }

  setCompanyType(companyType: string): void {
    localStorage.setItem('wsc-cloud-company-type', companyType);
  }

  getCompanyType(): string | null {
    return localStorage.getItem('wsc-cloud-company-type');
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  logout(): void {
    localStorage.removeItem('wsc-cloud-sctoken');
    localStorage.removeItem('wsc-cloud-username');
    localStorage.removeItem('wsc-cloud-role');
    localStorage.removeItem('wsc-cloud-site');
    localStorage.removeItem('wsc-cloud-last-action');
    localStorage.removeItem('wsc-cloud-img-user');
    localStorage.removeItem('wsc-cloud-user-id');
    localStorage.removeItem('wsc-cloud-company-type');
    localStorage.removeItem('wsc-cloud-role-id');
    localStorage.removeItem('wsc-cloud-name');
    this.router.navigate(['auth/login']);
    // window.location.reload()
  }

  login$(username: string, password: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    let body = {
      username: username,
      password: password,
      platform: "WEB"
    };

    return this.http.post(this.REST_API_SERVER + 'login', body, httpOptions);
  }
}
