import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { map, catchError, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication/authentication.service';
import { environment } from '../../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private auth: AuthenticationService) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token: any = this.auth.getToken();

    if (token) {
      // request = request.clone({ headers: request.headers.set('xAuth', 'Bearer ' + token) });
      request = request.clone({
        headers: request.headers.set('Accept', 'application/json')
          .set('Authorization', 'Bearer ' + token)
      });
    }

    return (
      next
        .handle(request)
        .pipe(
          tap(null, (error) => {
            if (error.status === 401) {
              // this.auth.logout();
            }
          })
        )
    );
  }
}
