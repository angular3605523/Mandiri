import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import {EmployeesService} from 'src/app/core/services/employees/employees.service'
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: '[list]',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [NgbModalConfig, NgbModal, MessageService,  CurrencyPipe],
})
export class ListComponent implements OnInit {
  public active1 = 1;
  public tanggal = new Date();
  public salary:number
  public minimumDate = new Date();
  public isLoading:boolean = true
  public deleteForm: FormGroup;
  public editDataUser: FormGroup;
  public namaQuery:string = ''
  public gajiQuery:string = ''
  public emailQuery:string = ''
  public groupQuery:string = ''
  customers1: any[];

    customers2: any[];

    selectedCustomer1: any;

    selectedCustomer2: any;
  constructor(
    private currencyPipe: CurrencyPipe,
    public config: NgbModalConfig, 
    private fb: FormBuilder,
    private modalService: NgbModal,
    private employeeService: EmployeesService,
    private router: Router,
    private messageService: MessageService
  ) {
    config.backdrop = 'static';
		config.keyboard = false;

    this.deleteForm = this.fb.group({
      email: [null]
    });

    this.editDataUser= this.fb.group({
      username: [null],
      email: [null],
      basicSalary: [null],
      age:[null],
    });
  }

  ngOnInit(): void {
      this.isQuery()
      this.loadData()
  }
  loadData(){
    setTimeout(() => {
      this.isLoading = false
      // Check apakah data pada Local Session kosong?
      if(sessionStorage.getItem("dataList") == null){
        // Jika kosong maka ambil Json dari file
        this.employeeService.getCustomersMedium().subscribe((data) => {
            this.customers1 = data.data
            // Taro ke session biar selanjutnya ambil dari session aja
            sessionStorage.setItem('dataList', JSON.stringify(data));
        });

      }else{
        let data = JSON.parse(sessionStorage.getItem("dataList"));
      this.customers1 = data.data;

      }
        
    }, 700);
  }
  hapus() {
    // Ambil data dari session
    let dataObj = JSON.parse(sessionStorage.getItem('dataList') || '{}'); // Ambil objek dari session
    let dataList = dataObj.data || []; // Ambil array dari properti "data" atau sebuah array kosong jika properti "data" tidak ada di objek
    // Carikan indeks data by email
    const index = dataList.findIndex((data: any) => data.email === this.deleteForm.controls['email'].value);

    if (index !== -1) {
      // Hapus data dengan indeks yang ditemukan
      dataList.splice(index, 1);

      // Simpen data yang sdh diubah ke session
      sessionStorage.setItem('dataList', JSON.stringify(dataObj));
      this.messageService.add({severity:'success',  summary: 'User Deleted', detail: 'User berhasil dihapus dari List'});
      this.loadData();
    }

  }

  ubahData() {

    // Ambil data dari session
    let dataObj = JSON.parse(sessionStorage.getItem('dataList') || '{}'); // Ambil objek dari session
    let dataList = dataObj.data || []; // Ambil array dari properti "data" atau sebuah array kosong jika properti "data" tidak ada di objek
    // Carikan indeks data by email
    const userIndex = dataList.findIndex(user => user.email === this.editDataUser.controls['email'].value);

    if (userIndex !== -1) {
      // Hapus data dengan indeks yang ditemukan
      dataList[userIndex].username = this.editDataUser.get('username').value;
      dataList[userIndex].email = this.editDataUser.controls['email'].value;
      dataList[userIndex].basicSalary = this.editDataUser.get('basicSalary').value;
      dataList[userIndex].age = this.editDataUser.get('age').value;

      // Simpen data yang sdh diubah ke session
      sessionStorage.setItem('dataList', JSON.stringify(dataObj));
      this.messageService.add({severity:'success',  summary: 'Data Updated', detail: 'Data berhasil diperbaharui dari List'});
      this.loadData();
    }

  }


  getData(email: string) {
    // Ambil data dari session
    let dataObj = JSON.parse(sessionStorage.getItem('dataList') || '{}'); // Ambil objek dari session
    let dataList = dataObj.data || []; // Ambil array dari properti "data" atau sebuah array kosong jika properti "data" tidak ada di objek 

    const data = dataList.find((data: any) => data.email === email);
    console.log(data)
    this.editDataUser.patchValue({
      username: data.username,
      email: data.email,
      basicSalary:  data.basicSalary,
      age: data.age
    });
  }
  open(val, email) {
    this.modalService.open(val, {centered: true });
    this.deleteForm.controls['email'].setValue(email);
  }
  openXL(val) {
      this.modalService.open(val, { size: 'xl', centered: true });
  }

  cantEdit(){
  this.messageService.add({severity:'warn',  summary: 'Action Prohibited !', detail: 'Tidak memiliki akses untuk ubah Value'});
  }
	queryParams(val, id:null) {
    if(val == 'employee'){
      this.router.navigate(["/admin/dashboard/index"], {
        queryParams: {
        create: val
        },
      });
    }else if(val == 'user'){
      this.router.navigate(["/admin/dashboard/index"], {
        queryParams: {
        detail: val,
        id: id
        },
      });
    }
  }


  // Feature Untuk user agar ketika search dan meninggalkan halaman, search ga hilang
  isQuery(){
    if(localStorage.getItem('namaQuery') != null){
    this.namaQuery = localStorage.getItem('namaQuery')
    }
    if(localStorage.getItem('gajiQuery') != null){
    this.gajiQuery = localStorage.getItem('gajiQuery')
    }
    if(localStorage.getItem('emailQuery') != null){
    this.emailQuery = localStorage.getItem('emailQuery')
    }
    if(localStorage.getItem('groupQuery') != null){
    this.groupQuery = localStorage.getItem('groupQuery')
    }
  }
  ngOnDestroy() {
    localStorage.setItem('namaQuery', this.namaQuery);
    localStorage.setItem('gajiQuery', this.gajiQuery);
    localStorage.setItem('emailQuery', this.emailQuery);
    localStorage.setItem('groupQuery', this.groupQuery);
  }
  
  
}
