import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {EmployeesService} from 'src/app/core/services/employees/employees.service';
import { MessageService } from 'primeng/api';
@Component({
  selector: '[create]',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [MessageService],
})
export class CreateComponent implements OnInit {
  category: any[];
  status: any[];
  selectedcategory: any;
  selectedstatus: any;
  form: FormGroup;
  public validate = false;
  public isValidURL:string
  public tanggal = new Date();
  public description = new Date();
  public loading:boolean = false
  public minimumDate = new Date();
  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private employeeService: EmployeesService,
    private router: Router) { 
      this.category = [
        { name: 'Department Finance', value: 'Department Finance' },
        { name: 'Department Operation', value: 'Department Operation' },
        { name: 'Department IT', value: 'Department IT' },
        { name: 'Department HR', value: 'Department HR' },
        { name: 'Department Sales', value: 'Department Sales' },
        { name: 'Department Support', value: 'Department Support' },
        { name: 'Department Risk', value: 'Department Risk' },
        { name: 'Department Accounting', value: 'Department Accounting' },
        { name: 'Department Secretary', value: 'Department Secretary' },
        { name: 'Department Public Relation', value: 'Department Public Relation' },
      ];

      this.status = [
        { name: 'Active', value: true },
        { name: 'Not Active', value: false},
      ];

      this.form = this.fb.group({
        username: [null, Validators.required],
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        email: [null, [Validators.required, Validators.email]],
        birthDate: [null, Validators.required],
        basicSalary: [null, Validators.required],
        status: [null, Validators.required],
        group: [null, Validators.required],
        age:[null, Validators.required],
        description: [null, Validators.required]
      });
    }
    

  ngOnInit(): void {
    // Ambil Params dari URL untuk check validation (Security Reason)
    this.route.queryParams.subscribe({
      next: (params) => {
       this.isValidURL = params.create
      },
    });
  }
  cancel() {
    this.router.navigate(['/admin/dashboard/index']);
  }

  onSubmit(){
    this.loading = true
      // Ubah Format Waktu sesuai JSON Template
      const date = new Date(this.form.controls['birthDate'].value);
      const year = date.getFullYear();
      const month = ("0" + (date.getMonth() + 1)).slice(-2);
      const day = ("0" + date.getDate()).slice(-2);
      const hours = ("0" + date.getHours()).slice(-2);
      const minutes = ("0" + date.getMinutes()).slice(-2);
      const seconds = ("0" + date.getSeconds()).slice(-2);
      const birtDateFormated = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
      
      // Ubah Format Waktu sesuai JSON Template
      const date1 = new Date(this.form.controls['description'].value);
      const year1 = date1.getFullYear();
      const month1 = ("0" + (date1.getMonth() + 1)).slice(-2);
      const day1 = ("0" + date1.getDate()).slice(-2);
      const hours1 = ("0" + date1.getHours()).slice(-2);
      const minutes1 = ("0" + date1.getMinutes()).slice(-2);
      const seconds1 = ("0" + date1.getSeconds()).slice(-2);
      const descriptionFormated = `${year1}-${month1}-${day1} ${hours1}:${minutes1}:${seconds1}`;

      const d = new Date();
      let umur =  d.getFullYear() - year;
      this.form.controls['age'].setValue(umur);

    if(this.form.valid){
      // Submit Form
      
      setTimeout(() => {
      this.form.controls['birthDate'].setValue(birtDateFormated);
      this.form.controls['description'].setValue(descriptionFormated);
      console.log(this.form.value)
      this.employeeService.addData(this.form.value).subscribe((data) => {
            console.log(data);
            sessionStorage.setItem('dataList', JSON.stringify(data));

      });
      this.messageService.add({severity:'success',  summary: 'Berhasil Tambah Data', detail: 'Data berhasil masuk database'});
      this.loading = false
      }, 1000);
      setTimeout(() => {
      this.router.navigate(['/admin/dashboard/index']);
      }, 3000);
    }else{
      console.log(this.form.value)
      this.loading = false
      this.validate = true
    }
  }
}
