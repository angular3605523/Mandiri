import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ContentComponent } from "./shared/components/layout/content/content.component";
import { FullComponent } from "./shared/components/layout/full/full.component";
import { full } from "./shared/routes/full.routes";
import { content } from "./shared/routes/routes";

import { AdminGuard } from './shared/guard/admin.guard';

import { AuthGuard } from 'src/app/core/guards/auth.guard'

const routes: Routes = [
  {
    path: 'admin',
    redirectTo: 'admin/dashboard/index',
    pathMatch: 'full',
    
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: '',
    loadChildren: () => import('./modules/landing-page/landing-page.module').then(m => m.LandingPageModule)
  },
  {
    path: 'admin',
    component: ContentComponent,
    canActivate: [AuthGuard],
    children: content
  },
  {
    path: 'admin',
    component: FullComponent,
    canActivate: [AuthGuard],
    children: full
  },
  {
    path: '**',
    redirectTo: 'admin/error-page/error-404'
  }
];

@NgModule({
  imports: [[RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'legacy'
})],
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
